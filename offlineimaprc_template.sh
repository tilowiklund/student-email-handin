#!/bin/bash

echo "[general]
accounts = Email

ui = Noninteractive.Basic

[Account Email]
postsynchook = /postsync.pl
autorefresh=5
quick=0
localrepository = EmailLocalMaildirRepository
remoterepository = EmailServerRepository

[Repository EmailLocalMaildirRepository]
type = Maildir
localfolders = /email/

[Repository EmailServerRepository]
type = IMAP
remotehost = ${EMAILSERVER}
remoteuser = ${EMAILUSER}
remotepass = ${EMAILPASS}
ssl = yes
folderfilter = lambda folder: folder in ['${INFOLDER}','${DRAFTFOLDER}']
sslcacertfile = /etc/ssl/certs/ca-certificates.crt
"
