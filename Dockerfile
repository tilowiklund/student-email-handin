FROM phusion/baseimage:0.9.18
CMD ["/sbin/my_init"]

RUN apt-get update &&\
    apt-get install -y maildir-utils offlineimap cpanminus make shared-mime-info &&\
    cpanm Capture::Tiny Mail::Box Path::Tiny Sys::RunAlone File::MimeInfo &&\
    mkdir /email /received /scripts /etc/service/autoemail &&\
    chmod a+rwx /email /received /scripts &&\
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD autoemail.sh /etc/service/autoemail/run
ADD offlineimaprc_template.sh /offlineimaprc_template.sh
ADD postsync.pl /postsync.pl

