#!/bin/sh
if [ ! -f /offlineimaprc ]; then
    /offlineimaprc_template.sh > /offlineimaprc
fi

offlineimap -c /offlineimaprc -l /imap.log
