#!/usr/bin/env perl
use strict;
use warnings;
use diagnostics;

our $VERSION = 0.0.1;

use Mail::Box;
use Mail::Box::Maildir;
use Mail::Box::Manager;

use File::MimeInfo::Magic;

use File::Temp;
use File::Path;
use File::Find;

use Path::Tiny;

use Capture::Tiny ':all';

use Sys::RunAlone;

my $mgr = Mail::Box::Manager->new;

sub make_safe_path {
    my $path = shift;
    return $path =~ s/\//./rgxms;
}

my $inbox = Mail::Box::Maildir->new(
    accept_new => 1,
    folder => '/email/' . (make_safe_path $ENV{'INFOLDER'}),
    manager => $mgr,
    access => 'rw' );

my $outbox = Mail::Box::Maildir->new(
    accept_new => 1,
    folder => '/email/' . (make_safe_path $ENV{'DRAFTFOLDER'}),
    manager => $mgr,
    access => 'a' );

my @scripts = ();
find(\&g, '/scripts');
sub g {
    if(-f) {
        my $script = "/scripts/$_";
        push @scripts, $script;
        return;
    }
}

foreach my $mail ($inbox->messages('!seen')) {
    print "\nProcessing message '${\$mail->subject}' from '${\$mail->sender->format}' in '${\$mail->filename}'\n";

    my $from     = $mail->sender->format;
    my $subject  = $mail->subject;
    my $filename = $mail->filename;
    my $tmpattachdir = File::Temp::tempdir('attachmentsXXXXX', DIR => '/tmp/');
    system 'mu', 'extract', $filename, '-a', '--target-dir=' . $tmpattachdir;

    my @stdouts = ();
    my @stderrs = ();
    my @exits   = ();

    my @tmpattachdirsout = ();
    foreach my $script (@scripts) {
        my $thistmpattachdirout = File::Temp::tempdir('attachmentsoutXXXXX', DIR => '/tmp/');
        print "\n Running '$script'... "; # '$from' '$subject'\nWith attachments in '$tmpattachdir'\n";
        my ($thisstdout, $thisstderr, $thisexit) = capture {
            # system('bash', $script, $from, $subject, @attachments);
            system 'bash', $script, $from, $subject, $tmpattachdir, $thistmpattachdirout;
        };
        my $thisexitcode = $thisexit >> 8;
        if($thisexitcode != 0) {
            print "which failed with code $thisexitcode\n";
            print $thisstdout;
            print $thisstderr;
        } else {
            print "which was successful\n";
            print $thisstderr;
        }
        push @stdouts, $thisstdout;
        push @stderrs, $thisstderr;
        push @exits, $thisexitcode;
        push @tmpattachdirsout, $thistmpattachdirout;
    }

    my $message;
    foreach(0..$#stdouts) {
        $message = $message . "$stdouts[$_]\n";
    }

    if(grep {$_ != 0} @exits) {
        print "There were some errors when running tests, leaving mail untouched\n";
    } else {
        print "All tests run, generating response\n";

        my $reply = $mail->reply(include => 'ATTACH', prelude => $message);
#        my $newbody = $reply->decoded;
        my $newbody = $reply->body;
        print "Attachment directories: @tmpattachdirsout \n";
        foreach my $thistmpaattachdir (@tmpattachdirsout) {
            opendir my $dir, $thistmpaattachdir or die "Cannot open directory: $!";
            my @files = readdir $dir;
            closedir $dir;
            foreach my $file (@files) {
                my $realpath = "$thistmpaattachdir/$file";
                if(-f $realpath) {
                    print "Attaching file '$file' (", File::MimeInfo::Magic::mimetype($realpath), ")\n";
                    $newbody = $newbody->attach(Mail::Message::Body::File->new(file => $realpath, mime_type => File::MimeInfo::Magic::mimetype($realpath))->encode(transfer_encoding => 'base64'));
                } else {
                    print "Skipping non-regular file: $file \n";
                }
            }
        }
        my $finalreply = Mail::Message->new(head => $reply->head, body => $newbody);
        $finalreply->label(draft => 1);
        $outbox->addMessage($finalreply);
        $finalreply->label(draft => 1);

        $mail->label(seen => 1);
    }

    File::Path::remove_tree($tmpattachdir);
}

$mgr->closeAllFolders;

__END__
